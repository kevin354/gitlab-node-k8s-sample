# GitLab Node K8s Deploy

## Build docker image

You can build and run a .NET-based container image using the following instructions:

```console
docker build -t express .
docker run --rm -it -p 3000:80 express
```

---

## Setup GCP

### 1. Setup project_id

  ```bash
  gcloud config set project <project_id>
  ```

### 2. Enable compute, container service

  ```bash
  gcloud services enable compute.googleapis.com
  gcloud services enable container.googleapis.com
  ```

### 3. Create service accounts key

3.1 在 project 的 IAM / Admin > Service accounts > Create service account， 將 Role 設定為:

  * Kubernetes Engine > Kubernetes Engine Developer
  * Cloud Storage > Storage Admin

3.2 建立完 service account 後，建立 Key(JSON)

  ```bash
gcloud iam service-accounts create gitlab-ci \
  --description="gitlab ci" --display-name="gitlab-ci"

gcloud projects add-iam-policy-binding gitlab-node-k8s-deploy \
    --member="serviceAccount:gitlab-ci@<project_id>.iam.gserviceaccount.com" \
    --role="roles/storage.admin"

gcloud projects add-iam-policy-binding gitlab-node-k8s-deploy \
    --member="serviceAccount:gitlab-ci@<project_id>.iam.gserviceaccount.com" \
    --role="roles/container.developer"

gcloud iam service-accounts keys create ~/Desktop/gitlab-ci.json \
    --iam-account gitlab-ci@<project_id>.iam.gserviceaccount.com
  ```


## GKE Cluster

### 1.1. Create Cluster

  ```bash
  gcloud container clusters create <cluster_name> \
    --zone asia-east1-b \
    --num-nodes=1
  ```

### 1.2. Edit GCR image url

Deployment > spec > template > spec > containers > image

  ```
  image: asia.gcr.io/<gcp_project_id>/<image_name>:latest
  ```

### 1.3. Setup K8s service and deployment

  ```
  kubectl apply -f deployment.yaml
  ```

## Setup GitLab CI

### 1. Setting CI/CD Variables

  ```
  GCP_SERVICE_KEY
  GCP_PROJECT
  GCP_ZONE
  GCP_CLUSTER_NAME
  GCP_GCR
  ```

## Clean up

```
gcloud container clusters delete <cluster_name>
```
